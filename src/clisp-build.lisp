#|

 2021/04/14 nightly build of clisp and then ap5

cd /home/don/clisp/build-mt/full/ 
# location of nightly build of clisp
# ap5dir is location of ap5 source and bin dirs
(./lisp.run -M ./lispinit.mem \
 -i $ap5dir/source/clisp-build.lisp \
 -x "(ap5::recompile)" -- top $ap5dir/  && \
 ./lisp.run -M ./lispinit.mem \
 -i $ap5dir/source/clisp-build.lisp \
 -x "(ap5::load-save)" -- top $ap5dir/)  >> transcript 2>&1

 2021/04/14 similarly for sbcl but built from installed sbcl
...
(defun c (&rest s) (apply 'concatenate 'string s))
;; *ap5-dir* is location of ap5 source and bin dirs
;; *clisp-build* is this file

...
(when make-succeeded
  (format t "~%~%recompile ap5~%~%")
  ;; using just built (not normally installed!) sbcl
  (sb-ext:run-program
   *built-sbcl*
   (list "--noinform" "--core" *built-core*
         "--load" *clisp-build*
         "--eval" "(ap5::recompile)" "--" "top" *ap5-dir*)
   :output *transcript-recompile* :error :output
   :if-output-exists :supersede)
  (tail *transcript-recompile*))

(defvar recompile-succeeded
  (= 0
     (sb-ext:process-exit-code
      (sb-ext:run-program
       "/usr/bin/grep"
       (list (c "Loading " *ap5-dir* "source/doc.lsp")
             (c *ap5-dir* "transcript-recompile"))))))

(when recompile-succeeded
  (format t "~%~%load-save ap5~%~%")
  (sb-ext:run-program
   *built-sbcl*
   (list "--noinform" "--core" *built-core*
         "--eval" (c "(require \"sb-bsd-sockets\" \"" *sbcl-dir* 
                     "obj/sbcl-home/contrib/sb-bsd-sockets.fasl\")")
         "--eval" (c "(require \"uiop\" \"" *sbcl-dir*
                     "obj/sbcl-home/contrib/uiop.fasl\")")
         "--eval" (c "(require \"asdf\" \"" *sbcl-dir* 
                     "obj/sbcl-home/contrib/asdf.fasl\")")
         "--load" *clisp-build*
         "--eval" "(ap5::load-save)" "--" "top" *ap5-dir*)
   :output "/home/don/sbcl/transcript-load-save" :error :output
   :if-output-exists :supersede)
  (tail "/home/don/sbcl/transcript-load-save"))

abcl: see file abcl-build (which uses this one)

|#
#+ignore ;#+sbcl  ;; 2022-04-26 (in desperation)
(progn
  (format t "optimization:~a" sb-c::*policy*)
  (proclaim '(optimize (debug 3) (safety 2) (speed 0) (space 1)))
  (format t "optimization:~a" sb-c::*policy*))
(in-package :cl-user) 
(defpackage "AP5" (:use "CL") (:nicknames "ap5")) 
(in-package :ap5) 

;; compatibility between clisp and sbcl and abcl
(defvar *args*
  #+abcl EXT:*COMMAND-LINE-ARGUMENT-LIST*
  #+sbcl sb-ext:*posix-argv*
  #+clisp ext:*args*
  #-(or clisp sbcl abcl) (error "don't know how to read command line args"))
#+(or abcl sb-thread) (push :mt *features*)

(format t "*args* = ~a" *args*)

(defparameter *top*
  (or (cadr (member "top" *args* :test 'string=))
      (error "need ap5 directory location")))
(defparameter *lit*
  #+abcl "ABCL" ;; lisp-implementation-type = "Armed Bear Common Lisp"
  #-abcl (lisp-implementation-type))
(defparameter *liv*
  (let ((liv (lisp-implementation-version))) 
    (or (cadr (member "liv" *args* :test 'string=))
	(subseq liv 0 (position #\Space liv)))))
#+sbcl ;; avoid separate dirs and dump names for sbcl versions
 ;; 2.1.8.4-38f1470 and 2.1.8.4-38f1470
 ;; not so bad to separate 2.1.8 from 2.1.7
 ;; but maybe later reduce to only 2.1 vs 2.2 ?
(setf *liv*
      (let ((liv (lisp-implementation-version)))
	(subseq liv 0
		(loop with count = 0 for i from 0 when
                      (and (or (>= i (length liv)) (eql (aref liv i) #\.))
			   (= (incf count) 3)) do (return i)))))
(defparameter *bin* 
  (make-pathname 
   :directory (append (pathname-directory *top*) 
		      (list (concatenate 'string "bin-" *lit*
                                         *liv* #+mt "-MT")))
   :defaults *top*))
(load (merge-pathnames "source/compile-.lsp" *top*)) 
(setf source-default-path (merge-pathnames "source/foo.lsp" *top*) 
       bin-default-path (merge-pathnames "foo.fas" *bin*)) 
(ensure-directories-exist bin-default-path :verbose t) 
(defun recompile()(compile-ap5 :recompile t))

#+(or clisp sbcl) ;; abcl currently has no save!
(defun load-save()
  (load-ap5) 
  #+clisp
  (ext:saveinitmem (format nil "/tmp/ap5-clisp-~a~a" *liv* #+mt "MT" #-mt "")
		   :executable t :norc t :documentation "AP5")
  #+sbcl
  (sb-ext:save-lisp-and-die (format nil "/tmp/ap5-sbcl-~a~a" *liv* #+mt "MT" #-mt "")
                            :executable t))

#+ignore 
(ext:fcase equal ext:*args* 
   ((("recompile")) (compile-ap5 :recompile t)) 
   ((("savemem")) (load-ap5) 
    (ext:saveinitmem (merge-pathnames "ap5" *bin*) :executable t :norc t 
        :documentation "AP5")) 
   ((NIL) (format t "usage: ~S [recompile|load]~%" *load-truename*)) 
   (t (error "invalid arguments: ~S" ext:*args*))) 
